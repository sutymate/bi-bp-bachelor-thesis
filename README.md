# Evacution simulator

Simulator implemented by [**Matej Šutý**](mailto:mato.suty@gmail.com "**Matej Šutý**") for thesis *Conflict solution in cellular models*.

#### Features
 - cellular automaton
 - multi-agent modeling
 - batch running
 - web interface
 - data analysis

-----
### Thesis
The text of thesis can be found in folder *report*.

### Implementation
TBA

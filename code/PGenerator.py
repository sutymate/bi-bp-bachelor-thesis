import random


def mdist(agent, gate):
    """
    :param agent: location tuple (x, y) of agent
    :param gate: location tuple (x, y) of exit/gate
    :return: manhattan distance (int)
    """
    return abs(agent[0] - gate[0]) + abs(agent[1] - gate[1])


class PGenerator:
    """
    Class used for generating locations and assigning aggressivity to agents.
    Generator uses deterministic RNG for repeatable run. Use for batch runs
    and comparison.
    """
    def __init__(self, width, height, gate, start_pos='far', seed=0):
        """
        Generates locations and aggressivity to each agent according to
        seed.
        :param width: width of room
        :param height: height of room
        :param gate: location tuple (x, y) of exit
        :param start_pos: spawn area of agents
        :param seed: used for deterministic rng
        """
        self.starting_position = start_pos
        self.seed = seed
        static = []
        for x in range(width):
            for y in range(height):
                # assign distance to exit to each position (x, y)
                static.append((mdist((x, y), gate), (x, y)))

        # sort distance in ascending order
        static.sort(key=lambda item: item[0])
        self.q = []
        random.seed(seed)
        # cells furthest away are chosen first, assign aggressivity
        granularity = 10
        if self.starting_position == "far":
            for cell in static[::-1]:
                self.q.append((cell[1], random.randrange(1, granularity)/granularity))
        # cells closest are chosen first, assign aggressivity
        if self.starting_position == "close":
            for cell in static:
                self.q.append((cell[1],
                               random.randrange(1, granularity)/granularity))

    def get_values(self, n=1):
        """
        Return location and aggressivity for n agents.
        :param n: number of agents
        :return: list of tuples with position and aggressivity ((x, y), a)
        """
        if n < 1:
            return []
        if len(self.q) == 0:
            return []
        agents = self.q[:n]
        return agents

    def get_starting_pos(self):
        """
        :return: spawn area of agents
        """
        return self.starting_position

    def get_seed(self):
        """
        :return: seed for rng
        """
        return self.seed

    def __repr__(self):
        return "<PGenerator seed:" + str(self.seed) + ">"

    def __str__(self):
        """
        Saves configuration of each run of model, provides timestamp.
        """
        import time
        dbg = time.asctime(time.localtime()) + '\n'
        dbg += "Seed:" + str(self.seed) + "\n"
        return dbg

import colormap as cmap
import numpy as np

"""
Agent is color NORMAL if no other color overrides this color.
"""
NORMAL = cmap.Color("Green").hex
"""
Agent is color AGGRESSIVE if his aggressivity is higher than 0.5
"""
AGGRESSIVE = cmap.Color("Red").hex
"""
Agent is color WAITING if his bonded agent did not move
"""
WAITING = cmap.Color("Violet").hex
"""
Agent ic color BLOCKED if due to friction all agents entering same cell don't 
move
"""
BLOCKED = cmap.Color("Blue").hex
"""
Agent is color EXHAUSTED if his movement (mostly diagonal) put him out of time
frame
"""
EXHAUSTED = cmap.Color("Pink").hex


def moving_avg_agg(model):
    """
    moving average of aggressivity of last <n> rescued agents
    :return float [0, 1]
    """
    n = 5
    array = model.rescued
    start = max(len(array) - n, 0)
    cumulative = 0
    cnt = 0
    for i in range(start, len(array)):
        cnt += 1
        if array[i]:
            cumulative += array[i].aggressivity
    return cumulative / cnt


def moving_avg_flow(model):
    """
    rolling mean of rescued agents in time frame [now-n, now]
    :return float [0, 1]
    """
    n = 5
    array = model.rescued
    start = max(len(array) - n, 0)
    cumulative = 0
    cnt = 0
    for i in range(start, len(array)):
        cnt += 1
        if array[i]:
            cumulative += 1
    return cumulative / cnt


def j_out_global(model):
    """
    total average flow of pedestrians through exit in epoch
    :return float [0, 1]
    """
    cnt = 0
    for agent in model.rescued:
        if agent:
            cnt += 1
    return cnt / model.epoch


def epoch(model):
    """
    number of time frames (epoch)
    :return high Naturals
    """
    return model.epoch


def start_pos(model):
    """
    Spawn location of PAgents.
    :return string
    """
    return model.starting_position


def generator_seed(model):
    """
    Seed of RNG
    :return int seed
    """
    return model.generator.get_seed()


def friction_stay(model):
    """
    number of blocking occasions per round when all agents are trying to enter
    cell stayed and didn't move
    :return int blocks
    """
    cumulative = 0
    for cell in model.schedule.cells.values():
        cumulative += cell.agents_stayed
    return cumulative


def movements(model):
    """
    number of agents that moved in epoch
    :return int movements
    """
    cnt = 0
    agents = model.schedule.agents.values()
    for agent in agents:
        if agent.tag.startswith("Pedestrian"):
            cnt += agent.moved
    return cnt


def total_avg_steps(model):
    """
    Average of movements per rescued agent in this epoch.
    :return float
    """
    cumulative = 0
    for agent in model.rescued:
        if agent:
            cumulative += agent.n_steps
    return cumulative / model.num_agents


def movements_global(model):
    """
    Number of all movements in simulation.
    :return int
    """
    return model.movements_total


def blocks_total(model):
    """
    Number of all blocks in simulation
    :return int
    """
    return model.blocks_total


def avg_a(model):
    """
    Average aggressivity of agents on grid
    :return float [0, 1]
    """
    aggressivity = 0
    n_agents = 0
    agents = model.schedule.agents.values()
    for agent in agents:
        if agent.tag.startswith("Pedestrian"):
            aggressivity += agent.aggressivity
            n_agents += 1
    if n_agents == 0:
        return 0
    return aggressivity / n_agents


def total_avg_a(model):
    """
    Average aggressivity of agents on grid
    :return float [0, 1]
    """
    aggressivity = 0
    n_agents = 0
    for i in range(len(model.rescued)):
        if model.rescued[i]:
            aggressivity += model.rescued[i].aggressivity
            n_agents += 1
    if n_agents == 0:
        return 0
    return aggressivity / n_agents


def avg_speed(model):
    """
    Average speed of pedestrian on grid
    :return float [0, 1]
    """
    speed = 0
    n_agents = 0
    agents = model.schedule.agents.values()
    for agent in agents:
        if agent.tag.startswith("Pedestrian"):
            speed += agent.n_steps
            n_agents += 1
    if n_agents == 0:
        return 0
    return speed / (model.epoch * n_agents)


def total_avg_speed(model):
    """
    Average speed of rescued agents
    :return float
    """
    speed = 0
    n_agents = 0
    for i in range(len(model.rescued)):
        if model.rescued[i]:
            n_agents += 1
            speed += model.rescued[i].n_steps / (i + 1)
    if n_agents == 0:
        return 0
    return speed / n_agents


def last_speed(model):
    """
    Speed of last evacuated agent
    :return float [0, 1]
    """
    last = len(model.rescued) - 1
    if last < 1:
        return 0
    else:
        last_agent = model.rescued[-1]
        if last_agent:
            return last_agent.n_steps / model.epoch
        else:
            return 0


def last_tte(model):
    """
    Time to exit for last evacuated agent
    :return float
    """
    last = len(model.rescued) - 1
    if last < 1:
        return 0
    else:
        last_agent = model.rescued[-1]
        if last_agent:
            return last_agent.period * 10


def positions(model):
    """
    Returns binary np.array of occupation at given moment
    :param model: model of simulation at given moment
    :return:
    """
    agents = model.schedule.agents.values()
    width = model.width
    height = model.height
    grid = np.full((height, width), fill_value=0.0, dtype=float)
    for agent in agents:
        x, y = agent.pos
        grid[y][width - 1 - x] = 1.0
    return grid

from collections import OrderedDict
from typing import List, Optional

from mesa.agent import Agent
from mesa.model import Model
from mesa.time import BaseScheduler

from indicators import EXHAUSTED


class TScheduler(BaseScheduler):
    """
    Class based on mesa.time.StagedActivation
    Class handles time and movement procedures in model. Each step of model is
    divided to ordered stages which all agents execute.
    """

    def __init__(
            self,
            model: Model,
            stage_list: Optional[List[str]] = None,
    ) -> None:
        super().__init__(model)
        self.stage_list = ["step"] if not stage_list else stage_list
        self.stage_time = 1 / len(self.stage_list)
        self.agents = OrderedDict()
        self.p_runners = OrderedDict()
        self.cells = OrderedDict()
        self.k_period = 0.2
        self.time = 0.0

    def add(self, agent: Agent) -> None:
        """
        Adding PAgents to agenture. All agents execute all steps in order they
        were added. Cells execute only 'start'.
        """
        if agent.unique_id in self._agents:
            raise Exception(
                "Agent with unique id {0} already added to scheduler".format(
                    repr(agent.unique_id)
                )
            )
        if agent.tag.startswith("Pedestrian"):
            self.agents[agent.unique_id] = agent
            # initial - all agents will move
            self.p_runners[agent.unique_id] = agent
        if agent.tag.startswith("Cell"):
            self.cells[agent.unique_id] = agent

    def remove(self, agent: Agent) -> None:
        """
        :param agent: PAgent evacuated from room
        """
        del self.agents[agent.unique_id]

    def step(self) -> None:
        """
        Executes all the stages for all agents.
        All cells execute stage 'start' after all PAgents finish 'start'.
        """
        for stage in self.stage_list:
            for agent in self.p_runners.values():
                getattr(agent, stage)()  # Run stage
            if stage == 'start':
                for cell in self.cells.values():
                    getattr(cell, stage)()  # Run stage 'start'

        self.p_runners.clear()
        self.steps += 1
        self.time += self.k_period
        for agent in self.agents.values():
            if self.fits_time(agent):
                self.p_runners[agent.unique_id] = agent
            else:
                agent.color = EXHAUSTED

    def fits_time(self, pedestrian):
        """
        Prevents PAgents who move in diagonal motion from repeating this step
        many times since diagonal movements is 3/2 longer than nominal.
        :param pedestrian: PAgent deciding if can execute move
        :return: true if PAgent movement time is not out of time frame else false
        """
        return (self.time + self.k_period) > pedestrian.period

    @property
    def agents(self):
        return self._agents

    @agents.setter
    def agents(self, value):
        self._agents = value

from PCell import PCell
from indicators import BLOCKED


class PCell2(PCell):
    def __init__(self, unique_id, model, static, color):
        super().__init__(unique_id, model, static, color)

    def select_winning_agent(self):
        """

        :return: None if no PAgent will enter this cell. Else PAgent that will
        enter this cell
        """
        opponents = {}
        aggressivity_sum = 0.0
        max_agent = max(self.q, key=lambda pagent: pagent.aggressivity)
        max_a = max_agent.aggressivity
        for agent in self.q:
            aggressivity_sum += agent.aggressivity
        for agent in self.q:
            opponents[agent] = agent.aggressivity / aggressivity_sum
        if len(opponents) == 1:
            return opponents.popitem()[0]
        else:
            """
            With probability P = friction * ( 1 - max_a ) no PAgent will enter 
            this cell and all will be blocked.
            With  ( 1 - P ) probability one of the PAgents with same aggr. will
            be randomly chosen to enter this cell.
            """
            all_stay_probability = self.model.friction * (1 - max_a)
            one_moves_probability = 1 - all_stay_probability
            all_stay = self.random.choices([True, False],
                                           weights=[all_stay_probability,
                                                    one_moves_probability])[0]
            if all_stay:
                for agent in opponents:
                    agent.color = BLOCKED
                self.agents_stayed = True
                self.model.blocks_total += 1
                return None
            else:
                winner = self.random.choices(list(opponents.keys()), weights=list(opponents.values()))[0]
                return winner

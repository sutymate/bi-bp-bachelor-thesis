import colormap as cmap
import numpy as np

from mesa import Model
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid

import indicators as indi
import graphs
from PAgent import PAgent
from PCell import PCell
from PCell2 import PCell2
from PGenerator import PGenerator
from PScheduler import TScheduler


def mdist(agent, gate):
    return abs(agent[0] - gate[0]) + abs(agent[1] - gate[1])


class PModel(Model):
    """
    Class which holds all elements of simulation - grid, agents, scheduler...
    Model advances in steps by function step.
    """

    def __init__(self, n, width, height, gate=None, strategy="new", generator=None,
                 parameters=None, kS=None, kO=None, kD=None, friction=None,
                 rng_seed=None):
        """
        Instance generates and places all objects (grid, agents, cells...).
        Grid is rectangular with indexation from (0, 0) in lower left corner
        to (width, height) in upper right corner.
        Maximum of width * height PAgents can be in the room.
        :param n: number of PAgents - pedestrian for evacuation
        :param width: width of room
        :param height: height of room
        :param gate: exit for evacuation, recommended location along walls
        :param strategy: for calculating probability
        :param generator: deterministic RNG for locations and aggressivity, uses
        seed
        """
        # parametric constants
        super().__init__()
        self.strategy = strategy
        self.agent_parameters = {}
        self.friction = 0
        if parameters:
            self.agent_parameters = dict(parameters)
            self.friction = self.agent_parameters['F']
        if kS:
            self.agent_parameters['S'] = kS
        if kO:
            self.agent_parameters['O'] = kO
        if kD:
            self.agent_parameters['D'] = kD
        if friction:
            self.friction = friction
        self.diagonal_penalisation = (3 / 2)
        # model constants
        self.width = width
        self.height = height
        self.num_agents = n
        self.stages = ['start', 'scan', 'solve', 'step']
        if gate:
            self.gate = gate
        else:
            self.gate = (0, self.random.randrange(0, height))
        # model objects
        if generator:
            self.generator = generator
        else:
            seed = self.random.randrange(0, 9999999999)
            self.generator = PGenerator(self.width, self.height, self.gate,
                                        seed=seed)
        self.grid = MultiGrid(width, height, False)
        self.schedule = TScheduler(self, self.stages)
        self.datacollector = DataCollector(
            model_reporters={"position": indi.positions
                             },
            agent_reporters={})

        self.datacollectorNorm = DataCollector(
            model_reporters={
                            "moving_avg_agg": indi.moving_avg_agg,
                            "moving_avg_flow": indi.moving_avg_flow,
                            "avg_speed": indi.avg_speed,
                            "last_speed": indi.last_speed
                            },
            agent_reporters={})
        if rng_seed:
            self.seed = rng_seed
        else:
            self.seed = self.generator.get_seed()
        self.random.seed(self.seed)
        self.starting_position = self.generator.get_starting_pos()
        # statistics
        self.running = True
        self.rescued = []
        self.epoch = 0
        self.evacuated = 0
        self.movements_total = 0
        self.blocks_total = 0
        self.exhausted_agents = 0
        self.current_id = 0
        self.heatmap = []
        # initialization, first PAgents, then PCells
        self.set_pagents()
        self.set_cells()
        self.show_config()

    def get_agent(self, pos, tag):
        """
        :param pos: location tuple (x, y) of requested agent on grid
        :param tag: "Pedestrian" for PAgent, "Cell" for PCell
        :return: PAgent or PCell
        """
        for agent in self.grid.grid[pos[0]][pos[1]]:
            if agent.tag.startswith(tag):
                return agent
        return None

    def get_cell(self, pos):
        """
        :param pos: location tuple (x, y) of requested MultiGrid list of PAgent
        and PCell
        :return: list of PAgent and PCell at given location
        """
        return self.grid.grid[pos[0]][pos[1]]

    def set_cells(self) -> None:
        """
        Place PCells on MultiGrid with calculated distance to exit as static
        value. Represented also by shade of grey.
        """
        # create matrix with y rows, x columns of static value
        static = np.empty((self.height, self.width))
        for x in range(self.width):
            for y in range(self.height):
                static[y][self.width - 1 - x] \
                    = mdist((x, y), self.gate)
        max_static = np.max(static)
        step = 255 / max_static
        for x in range(self.width):
            for y in range(self.height):
                static_value = static[y][self.width - 1 - x]

                color = str(cmap.rgb2hex(int(static_value * step),
                                         int(static_value * step),
                                         int(static_value * step),
                                         normalised=False))
                if (x, y) == self.gate:
                    color = cmap.Color("Brown").hex
                cell = PCell(self.next_id(), self,
                             static_value, color)
                self.grid.place_agent(cell, (x, y))
                self.schedule.add(cell)

    def set_pagents(self):
        """
        Place n PAgents on MultiGrid and add them to PSchedule agenture. Assign
        them aggressivity based on generator.
        """
        heterogenity = False
        heterogenity_values = [0.1, 0.9]
        agent_values = self.generator.get_values(self.num_agents)
        cnt = 0
        half = len(agent_values) / 2
        for position_aggressivity in agent_values:
            pos = position_aggressivity[0]
            aggressivity = position_aggressivity[1]
            if heterogenity:
                if cnt < half:
                    self.agent_parameters['O'] = heterogenity_values[0]
                else:
                    self.agent_parameters['O'] = heterogenity_values[1]
                cnt += 1
            pedestrian_agent = PAgent(self.next_id(), self,
                                      aggressivity=aggressivity,
                                      agent_parameters=self.agent_parameters)
            self.grid.place_agent(pedestrian_agent, pos)
            self.schedule.add(pedestrian_agent)

    def leave(self, agent: PAgent):
        """
        If agent reaches exit, remove him from schedule and update statistics.
        :param agent: evacuated PAgent
        """
        self.grid.remove_agent(agent)
        self.schedule.remove(agent)
        self.evacuated += 1
        self.rescued.append(agent)

    def step(self):
        """Advance the model by one step."""
        self.epoch += 1
        self.schedule.step()
        if len(self.rescued) != self.epoch:
            self.rescued.append(None)
        # self.datacollectorNorm.collect(self)
        # if self.epoch % 32 == 1:
        #     print('epoch', format(self.epoch, '06'))
        if self.evacuated == self.num_agents:
            self.running = False

    def save_heatmap(self, filepath=None):
        """
        Save occupancy situation at given moment.
        :param filepath: if present, save at given location
        """
        self.heatmap.append(indi.positions(self))
        if filepath:
            graphs.heatmap_time(self.heatmap, filepath)

    def show_config(self):
        """
        Shows a simulation configuration in each run. Saves to log
        """
        dbg = self.generator.__str__()
        dbg += "Width:" + str(self.width) + "\n"
        dbg += "Height:" + str(self.height) + "\n"
        dbg += "gate:" + str(self.gate) + "\n"
        dbg += "num_agents:" + str(self.num_agents) + "\n"
        dbg += "strategy:" + str(self.strategy) + "\n"
        dbg += "position:" + str(self.starting_position) + "\n"
        if self.agent_parameters:
            dbg += str(self.agent_parameters)
        else:
            agent = PAgent(0, self, 0, 0)
            agent.k['F'] = self.friction
            dbg += str(agent.k)
        dbg += "\n"
        f = open("debug.txt", 'a')
        f.write(dbg)
        f.close()
        # print(dbg)

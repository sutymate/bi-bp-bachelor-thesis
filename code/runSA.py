import os
import time
import random
import itertools
import numpy as np
from numpy import arange, floor
import pandas as pd

from PModel import PModel
from PGenerator import PGenerator
import indicators as indi

if __name__ == '__main__':
    max_steps = 500
    simulations_per_setup = 5
    n = 70
    width = 15
    height = 15
    gate = (0, 8)
    strategy = "new"
    starting_position = "far"
    seed = 1245
    generator = PGenerator(width, height, gate, starting_position, seed)
    file_path = 'out/SA/'

    # float
    kS = [i for i in arange(2.0, 5.0, 0.25)]
    kO = [i for i in arange(0.3, 1.0, 0.1)]
    friction = [1 / 3, 2 / 3, 3 / 3]
    parameters = [kS, kO, friction]
    parameter_space = np.array(sorted([*(itertools.product(*parameters))]), order='K')
    parameter_space = parameter_space[:10]
    print(parameter_space)
    simulation_start = floor(time.mktime(time.localtime()))
    output = open(file_path + str(simulation_start) + '.csv', mode='a')
    headers = ["static", "occupancy", "diagonal", "friction",
               "simulation_number", "TET", "rng_seed", "n_agents", "width",
               "height", "gate_x", "gate_x", "cell_strategy",
               "starting_position", "pos_seed"]
    separator = ','
    headers = separator.join(headers) + '\n'
    output.write(headers)
    output.close()
    for parameter in parameter_space:
        parameters = {'S': parameter[0], 'O': parameter[1], 'D': 0.9, 'F': parameter[2]}
        for simulation_number in range(simulations_per_setup):
            rng_seed = time.time_ns()
            print(rng_seed)
            model = PModel(n, width, height, gate, strategy, generator, parameters, rng_seed=rng_seed)
            cnt = 0
            while model.running and cnt < max_steps:
                model.step()
                cnt += 1
            result = [parameters['S'], parameters['O'], parameters['D'], parameters['F'],
                      simulation_number, cnt, rng_seed, n, width, height, gate[0], gate[1], strategy,
                      starting_position, seed]
            output = open(file_path + str(simulation_start) + '.csv', mode='a')
            result = separator.join(map(str, result)) + '\n'
            output.write(result)
            output.close()

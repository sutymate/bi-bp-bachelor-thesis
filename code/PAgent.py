import math
from collections import OrderedDict

from mesa import Agent

from indicators import NORMAL, WAITING


# Pedestrian Agent
class PAgent(Agent):
    """
    Class representing pedestrian trying to evacuate room. Pedestrian has custom
    sensitivity parameters and aggressivity. Can move in 8 directions or stay in
    place. Pedestrian can enter only empty cell. In each step PAgent chooses
    his destination cell according to attraction. Attraction depends on static
    value (distance from cell to exit), occupancy and whether the move is in
    diagonal direction. Diagonal movement takes 3/2 of nominal movement.
    """

    def __init__(self, unique_id, model, aggressivity=None, agent_parameters=None):
        """
        :param unique_id: unique_id for PScheduler agenture index
        :param model: PModel which moves this PAgent
        :param aggressivity: assigned aggressivity in float range (0, 1)
        """
        super().__init__(unique_id, model)
        self.tag = "Pedestrian" + str(unique_id)
        # time duration of nominal movement (not diagonal)
        self.tau = self.model.schedule.k_period
        """
        time after movement is complete. Used for fitting into time frame of
        PScheduler
        """
        self.period = 0
        """
        aggressivity is the ability to win conflict when 2 or more agents
        attempt to enter the same cell
        """
        if aggressivity:
            self.aggressivity = aggressivity
        else:
            self.aggressivity = self.random.randrange(0, 10) / 10
        """
        sensitivity parameters:
        S for static, domain R, higher means more likely to move in direction to
        exit
        O for occupancy, domain [0, 1], lower means PAgents tries to enter
        occupied cell
        D for diagonal movement, domain [0, 1], lower allows PAgent to move in 
        diagonal direction
        """
        if agent_parameters:
            self.k = agent_parameters
        else:
            self.k = {
                "S": 3.5,
                "O": 0.5,
                "D": 0.9
            }
        """
        Destination cell in next time frame. If its occupied, agent enters only
        when occupant leaves.
        """
        self.next = None
        """
        Bond to PAgent which occupies the destination cell. When occupant leaves,
        this agents moves to destination cell. Otherwise stays
        """
        self.head = None
        """
        PAgent which tries to enter cell where this PAgent stays. If this PAgent
        moves, attempting PAgent moves to this cell.
        """
        self.tail = None
        """
        Visualization color based on aggressivity
        """
        self.color = NORMAL  # if self.aggressivity < 0.5 else AGGRESSIVE
        self.moved = False
        """
        step is each movement
        """
        self.n_steps = 0

    def start(self):
        """
        PAgents selects most attractive destination cell in his neighborhood.
        After all PAgents choose their destination cell PCell in each location
        chooses which PAgents enters the location.
        PAgent starts only if after previous epoch his time fits time frame of
        [kh, (k+1)h].
        :return:
        """
        self.head = None
        self.tail = None
        self.moved = False
        self.color = NORMAL  # if self.aggressivity < 0.5 else AGGRESSIVE
        self.next = self.best_fit()
        cell = self.model.get_agent(self.next, "Cell")
        self.next = None
        cell.add(self)

    def scan(self):
        """"" 
        PAgent which does not bond (moves to empty cell (self.head == None)
        moves first and then allows bound agents (self.tail) to enter his cell
        """""
        if self.head:
            pass
        else:
            if self.next:
                self.make_move()

    def solve(self):
        """""
        If PAgent was assigned a destination cell by PCell but bound agent 
        in the destination cell did not move color this agent as BLOCKED. Clear 
        states for next epoch.
        """""
        if not self.moved:
            self.update()
            if self.next:
                self.color = WAITING

    def update(self):
        """
        If agents movement resulted in time period out of frame this function
        will be called so that nominal time will be added.
        """
        self.period += self.tau
        self.model.exhausted_agents += 1

    def step(self):
        """
        When agents reaches evacuation point, remove him from PSchedule agenture
        and update statistics
        """
        if self.pos == self.model.gate:
            self.model.leave(self)
        self.next = None

    def best_fit(self):
        """
        Scan neighbourhood cells (including self) and return most attractive
        as destination cell
        :return: location tuple (x, y)
        """
        cells = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=True)
        # S(y), D(y), O(y) are in parameters for each position pos
        parameters = {}
        for pos in cells:
            static = 0
            occupation = 0
            agents = self.model.get_cell(pos)
            for agent in agents:
                if agent.tag.startswith("Cell"):
                    static = agent.static
                if agent.tag.startswith("Pedestrian"):
                    if pos == self.pos:
                        occupation = 0
                    else:
                        occupation = 1
            diagonal = self.is_diagonal(pos)
            parameters[pos] = {"S": static, "D": diagonal, "O": occupation}
        attraction = self.probability(parameters)
        # stochastic selection of destination cell, probability is attraction
        stochastic_selection = self.random.choices(list(attraction.keys()),
                                                   weights=list(attraction.values()))
        return stochastic_selection[0]

    def probability(self, evaluation):
        """
        Calculate attraction of each position of cells in :param evaluation
        based on values in parameters {S - static, O - occupancy, D - diagonal}.
        Use strategy from model - difference is in normalization and equation.
        :param evaluation: Dictionary of pos(key): parameters(value)
        :return: Dictionary of pos(key): attraction(value)
        """
        ks = self.k['S']
        ko = self.k['O']
        kd = self.k['D']

        # old strategy
        top = {}
        bottom_sum = 0
        # results
        attraction = OrderedDict()
        for pos in evaluation:
            # S belongs to [0, 1]
            S = evaluation[pos]['S']
            # O is 0 or 1
            Occupy = evaluation[pos]['O']
            # D is 0 or 1
            D = evaluation[pos]['D']
            # notice the ko parameter in this strategy
            top[pos] = math.exp((-ks) * S) * (1 - ko * Occupy) * (1 - kd * D)
            bottom_sum += top[pos]

        # new strategy, mixing P_s and P_o based od ko sensitivity
        P_s = {'top': {}, 'bottom_sum': 0}
        attraction_static = OrderedDict()
        P_o = {'top': {}, 'bottom_sum': 0}
        attraction_static_occupancy = OrderedDict()
        # results
        attraction_final = OrderedDict()
        for pos in evaluation:
            # S belongs to [0, 1]
            S = evaluation[pos]['S']
            # O is 0 or 1
            Occupy = evaluation[pos]['O']
            # D is 0 or 1
            D = evaluation[pos]['D']
            # notice the missing occupancy factor (ko and Occupy)
            P_s['top'][pos] = math.exp((-ks) * S) * (1 - kd * D)
            P_s['bottom_sum'] += P_s['top'][pos]
            # notice the missing ko parameter
            P_o['top'][pos] = math.exp((-ks) * S) * (1 - Occupy) * (1 - kd * D)
            P_o['bottom_sum'] += P_o['top'][pos]

        # normalize
        for pos in evaluation:
            attraction[pos] = top[pos] / bottom_sum
            attraction_static[pos] = P_s['top'][pos] / P_s['bottom_sum']
            attraction_static_occupancy[pos] = P_o['top'][pos] / P_o['bottom_sum']

        # ko belongs to [0,1]
        for pos in evaluation:
            attraction_final[pos] = ko * attraction_static_occupancy[pos] + (1 - ko) * attraction_static[pos]
        if self.model.strategy == "new":
            return attraction_final
        else:
            return attraction

    def make_move(self):
        """
        Update location, time and statistics after movement. Allow bounded PAgent to enter
        former cell.
        :return:
        """
        self.period += self.tau if not self.is_diagonal(self.next) else \
            self.tau * self.model.diagonal_penalisation
        self.model.movements_total += 1
        self.n_steps += 1
        self.model.grid.move_agent(self, self.next)
        if self.tail:
            self.tail.make_move()
        self.moved = True

    def is_diagonal(self, pos):
        """
        Indicator of diagonal movement from current position to :param pos.
        :param pos: destination cell
        :return: True if movement is in diagonal direction, False otherwise
        """
        if not pos:
            return False
        x = self.pos[0] - pos[0]
        y = self.pos[1] - pos[1]
        return x * y != 0

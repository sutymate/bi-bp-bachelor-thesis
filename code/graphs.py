from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import math


def heatmap_time(heatmap, filepath=None):
    """
    Shows or saves a heatmap in one epoch.
    :param heatmap: np.array of occupation at epoch
    :param filepath: if present, save to filepath 
    """
    grid = heatmap
    epoch = len(grid)
    previous = np.full(grid[0].shape, fill_value=0.0, dtype=float)
    for tick in grid:
        previous += tick
        previous *= tick
        max_val = np.max(previous)
        if max_val == 0:
            previous *= 0
        else:
            previous /= max_val

    a = previous
    fig1, ax1 = plt.subplots()
    heatmap = ax1.imshow(a, cmap='hot', interpolation='nearest')
    fig1.colorbar(heatmap)
    # fig1.colorbar.ax.set_ylabel
    fig1.suptitle("Epoch: " + str(epoch))
    if filepath:
        filename = filepath + "hmap_" + format(epoch, '06')
        fig1.savefig(filename)
        plt.close(fig1)
    else:
        fig1.show()


def sample_heatmap(heatmaps, filepath=None):
    """
    Creates a sequence of heatmaps in each epoch. Heatmaps cover the situation
    througout all iterations of same simulation. Auxiliary plots global flow
    and number of running simulations.
    :param heatmaps: list of simulation iterations. Each simulation iteration is
    a list of epoch heatmaps - binary np.arrays.
    :param filepath: preferred save location
    """
    max_epoch = len(max(heatmaps, key=len))
    n_iterations = len(heatmaps)
    out_hmap = []
    shape = heatmaps[0][0].shape
    top_occupancy = 0
    models_running = [0 for i in range(max_epoch)]
    for epoch in range(max_epoch):
        epoch_hmap = np.zeros(shape)
        for iteration in heatmaps:
            if epoch < len(iteration):
                models_running[epoch] += 1
                epoch_hmap += iteration[epoch]
        top_occupancy = max(top_occupancy, np.max(epoch_hmap))
        out_hmap.append(epoch_hmap)
    for epoch in range(max_epoch):
        out_hmap[epoch] /= top_occupancy
        gs = gridspec.GridSpec(2, 2)
        fig1 = plt.figure(figsize=(10, 10))
        ax1 = fig1.add_subplot(gs[0, 0])
        ax2 = fig1.add_subplot(gs[0, 1])
        ax3 = fig1.add_subplot(gs[1, :])
        a = out_hmap[epoch]
        heatmap = ax2.imshow(a, cmap='hot', interpolation='nearest', vmin=0, vmax=1)
        fig1.colorbar(heatmap, ax=ax2).ax.set_ylabel("avg. of agent presence")
        ax1.set_title('Models running')
        ax1.set_ylabel("# of models")
        ax1.set_xlabel("Epoch")
        ax2.set_title('Movement heatmap')
        ax2.set_ylabel("Width")
        ax2.set_xlabel("Height")
        ax3.set_title('Average exit flow')
        ax3.set_ylabel("avg. agents leaving")
        ax3.set_xlabel("Epoch")
        ax1.set_ylim([0, math.floor(n_iterations*1.1 + 2)])
        ax1.plot(models_running[:epoch])
        ax3.plot(flow(heatmaps, epoch))
        fig1.suptitle("Epoch: " + str(epoch))
        filename = filepath + "/hmap_" + format(epoch, '06') + '.pdf'
        fig1.savefig(filename)
        plt.close(fig1)
        # fig1.show()


def flow(heatmaps, max_epoch):
    """
    Used for plotting average flow throughout all iterations in simulation.
    :param heatmaps: list of iterations, each iteration has all epoch heatmaps
    :param max_epoch: plots all previous epoches up to max_epoch
    :return: list of avg flow per epoch
    """
    out_flow = [0 for i in range(max_epoch)]
    previous = []
    for idx in range(len(heatmaps)):
        previous.append(np.sum(heatmaps[idx][0]))
    for epoch in range(max_epoch):
        flow = 0
        for idx in range(len(heatmaps)):
            iteration = heatmaps[idx]
            if epoch < len(iteration):
                current = np.sum(iteration[epoch])
                flow += previous[idx] - current
                previous[idx] = current
        out_flow[epoch] = flow / len(heatmaps)
    return out_flow

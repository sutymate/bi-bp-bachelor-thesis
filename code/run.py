import time
from collections import OrderedDict
import math

import pandas as pd
import pickle
import matplotlib.pyplot as plt
import numpy as np
import random

from mesa.batchrunner import BatchRunner

from graphs import sample_heatmap, flow
import indicators as indi
from PGenerator import PGenerator
from PModel import PModel


def simpleHetero():
    """
    small room low number of agents. only for testing
    :return:
    """
    n = 70
    width = 15
    height = 16
    gate = (0, 8)
    parameters = {
        'S': 2,
        'O': 0.5,
        'D': 0.5,
        'F': 0.9
    }
    rng_seed = time.mktime(time.localtime())
    generator = PGenerator(width, height, gate, seed=1245)
    return PModel(n, width, height, gate, generator=generator, parameters=parameters, rng_seed=rng_seed)

def simple():
    """
    small room low number of agents. only for testing
    :return:
    """
    n = 1
    width = 3
    height = 3
    gate = (0, 0)
    return PModel(n, width, height, gate)


def medium():
    """
    medium room some agents. testing the longer run and various situations
    :return:
    """
    n = 20
    width = 10
    height = 10
    gate = (0, 5)
    return PModel(n, width, height, gate)


def large():
    """
    heavy testing, large room all situations may happen
    :return:
    """
    n = 200
    width = 30
    height = 30
    gate = (0, 15)
    seed = 1341
    generator = PGenerator(width, height, gate, seed=seed)
    return PModel(n, width, height, gate, generator=generator)


def batch_param_comb():
    """
    Runs a model number of time with set of fixed parameters and one selection
    from variable parameters for each run.
    :return:
    """
    # fixed parameters will be covered in simulation output
    n = 70
    width = 15
    height = 15
    gate = (0, 8)
    starting_position = 'far'
    seed = time.mktime(time.localtime())
    fixed_params = {
        "n": n,
        "width": width,
        "height": height,
        "gate": gate,
        "strategy": "new",
        "generator": PGenerator(width, height, gate, starting_position, seed)
    }
    # variable parameters will be present in simulation output
    generators = []
    for seed in range(0, 1000):
        generators.append(PGenerator(width, height, gate, starting_position, seed))
    agent_parameters = []
    """
    Granularity (should be >= 3) is the number of discrete elements equally
    distant for every sensitivity parameter
    Precision in tenths(10) or hundredths(100)
    Sample dimension is [static_min, static_max] X [0, 1] X [0, 1]
    for static, occupancy and friction in discrete steps of granularity
    """
    granularity = 5
    precision = 10
    static_min = 2
    static_max = 4
    for static_global in range(int(static_min * precision * granularity),
                               int(static_max * precision * granularity), precision):
        for occupancy_global in range(granularity):
            for diagonal_global in range(1):
                diagonal_fixed = 0.7
                for friction_global in range(granularity):
                    static_fixed = static_global / precision / granularity
                    occupancy_fixed = occupancy_global / granularity
                    friction_fixed = friction_global / granularity
                    for static in range(int(static_min * precision * granularity),
                                        int(static_max * precision * granularity),
                                        precision):
                        p = frozenset({
                                          "S": static / precision / granularity,
                                          "O": occupancy_fixed,
                                          "D": diagonal_fixed,
                                          "F": friction_fixed
                                      }.items())
                        agent_parameters.append(p)
                    for occupancy in range(granularity):
                        p = frozenset({
                                          "S": static_fixed,
                                          "O": occupancy / granularity,
                                          "D": diagonal_fixed,
                                          "F": friction_fixed
                                      }.items())
                        agent_parameters.append(p)
                    for friction in range(granularity):
                        p = frozenset({
                                          "S": static_fixed,
                                          "O": occupancy_fixed,
                                          "D": diagonal_fixed,
                                          "F": friction / granularity
                                      }.items())
                        agent_parameters.append(p)
    frozen_agent_parameters = frozenset(agent_parameters)
    print(len(frozen_agent_parameters))
    variable_params = {
        # "strategy": ["old", "new"],
        # "generator": generators,
        "parameters": frozen_agent_parameters
    }
    """
    iterations: The total number of times to run the model for each combination of parameters.
    max_steps: Upper limit of steps above which each run will be halted if it hasn't halted on its own.
    """
    mr1 = {
        "Global flow": indi.j_out_global,
        "avg of steps per a.": indi.total_avg_steps,
        "# of epoch": indi.epoch,
        "Last a. speed": indi.last_speed,
        "Total avg speed": indi.total_avg_speed,
        "Total avg agg": indi.total_avg_a,
        "Total moves": indi.movements_global,
        "Total blockings": indi.blocks_total,
        "Starting position": indi.start_pos
    }
    mr2 = {
        "positions": indi.positions
    }

    batch_runner = BatchRunner(
        PModel,
        variable_params,
        fixed_params,
        iterations=1,
        max_steps=500,
        model_reporters=mr2,
        agent_reporters={}
    )

    batch_runner.run_all()
    run_data = batch_runner.get_model_vars_dataframe()
    timestamp = int(time.time()).__str__()
    run_data.to_csv(("out/" + ("simulation3_" + timestamp) + ".csv"), index=False)


def stack100(df2):
    import matplotlib.pyplot as plt
    for strategy in ['new', 'old']:
        df = df2[df2['strategy'] == strategy]
        plt.xlabel('kO values')
        plt.ylabel('Transition probability')
        if strategy == "new":
            plt.title("New strategy")
        else:
            plt.title("Old strategy")
        plt.stackplot(df['ko'],
                      [df[i] for i in range(0, 9)],
                      labels=[str(i) for i in range(0, 9)],
                      alpha=0.8)
        for xc in df['ko']:
            plt.axvline(x=xc)

        plt.legend(loc=2, fontsize='large')
        plt.savefig("out/2ks=2,5 d=0.5 strategy=" + strategy + " ko=variable.pdf")
        plt.show()


def sampleEvaluation():
    data = {'ko': [],
            0: [],
            1: [],
            2: [],
            3: [],
            4: [],
            5: [],
            6: [],
            7: [],
            8: [],
            'strategy': []}
    for step in range(0, 11):
        index = [i for i in range(0, 9)]
        static = [4, 3, 4,
                  3, 2, 3,
                  2, 1, 2]
        occupancy = [0 for i in range(0, 9)]
        occupancy[6] = 1
        occupancy[7] = 1
        occupancy[8] = 1
        diagonal = [0 for i in range(0, 9)]
        diagonal[0] = 1
        diagonal[2] = 1
        diagonal[6] = 1
        diagonal[8] = 1
        ks = 2.5
        ko = step / 10
        kd = 0.5

        # old strategy
        # old strategy
        top = {}
        bottom_sum = 0
        # results
        attraction = {}
        for i in index:
            # S belongs to [0, 1]
            S = static[i]
            # O is 0 or 1
            Occupy = occupancy[i]
            # D is 0 or 1
            D = diagonal[i]
            # notice the ko paramater in this strategy
            top[i] = math.exp((-ks) * S) * (1 - ko * Occupy) * (1 - kd * D)
            bottom_sum += top[i]

        P_s = {'top': {}, 'bottom_sum': 0}
        attraction_static = {}
        P_o = {'top': {}, 'bottom_sum': 0}
        attraction_static_occupancy = OrderedDict()
        # results
        attraction_final = {}
        for i in index:
            # S belongs to [0, 1]
            S = static[i]
            # O is 0 or 1
            Occupy = occupancy[i]
            # D is 0 or 1
            D = diagonal[i]
            # notice the missing occupancy factor (ko and Occupy)
            P_s['top'][i] = math.exp((-ks) * S) * (1 - kd * D)
            P_s['bottom_sum'] += P_s['top'][i]
            # notice the missing ko parameter
            P_o['top'][i] = math.exp((-ks) * S) * (1 - Occupy) * (1 - kd * D)
            P_o['bottom_sum'] += P_o['top'][i]

        # normalize
        for i in index:
            attraction[i] = top[i] / bottom_sum
            attraction_static[i] = P_s['top'][i] / P_s['bottom_sum']
            attraction_static_occupancy[i] = P_o['top'][i] / P_o['bottom_sum']
        print(attraction)
        print(attraction_final)
        # ko belongs to [0,1]
        for i in index:
            attraction_final[i] = ko * attraction_static_occupancy[i] + (1 - ko) * attraction_static[i]
            # attraction_final[i] = math.floor(attraction_final[i] * 1000)
            # attraction[i] = math.floor(attraction[i] * 1000)
            attraction_final[i] = attraction_final[i]
            attraction[i] = attraction[i]
        print(attraction)
        print(attraction_final)

        data['ko'].append(ko)
        data['strategy'].append('new')
        for i in index:
            data[i].append(attraction_final[i])

        data['ko'].append(ko)
        data['strategy'].append('old')
        for i in index:
            data[i].append(attraction[i])

    # print("NEW\n", attraction_final)
    # print("OLD\n", attraction)
    t = time.asctime(time.localtime())
    df = pd.DataFrame.from_dict(data)  # , orient='index')
    df.to_csv("out" + str(t) + ".csv")
    stack100(df)


def batch_sample(settings, iterations, max_iterations=500, filepath='out/tmp'):
    n = settings['n']
    width = settings['width']
    height = settings['height']
    gate = settings['gate']
    strategy = 'new'
    starting_position = settings['starting_position']
    seed = settings['seed']
    if settings['generator']:
        generator = settings['generator']
    else:
        generator = PGenerator(width, height, gate, starting_position, seed)
    parameters = settings['parameters']
    heatmaps = []
    for i in range(iterations):
        print("\n\t\tITER", i)
        cnt = 0
        rng_seed = time.mktime(time.localtime())
        model = PModel(n, width, height, gate, strategy, generator, parameters, rng_seed=rng_seed)
        while model.running and cnt < max_iterations:
            model.step()
            model.save_heatmap(filepath)
            cnt += 1
        heatmaps.append(model.heatmap)
    sample_heatmap(heatmaps, filepath)


def n_dep(settings):
    agents = [i for i in range(10, 220, 10)]
    results = {}
    for n in agents:
        results[n] = []
    iterations = 50
    # ['1', '3', '33']
    # ['4', '55', '6']
    settings1 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '1'
    }}
    settings2 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '2'
    }}
    settings3 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '3'
    }}
    settings33 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '33'
    }}
    settings4 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '4'
    }}
    settings5 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '5'
    }}
    settings55 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '55'
    }}
    settings6 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '6'
    }}
    all_sets = [settings1, settings2, settings3, settings33, settings4, settings5, settings55, settings6]
    x = 0
    y = 0
    z = 0
    for one_set in all_sets:
        data = []
        container_avg = [15]
        container_n = []
        for n in agents:
            container_n = []
            avg_in_n = 0
            for i in range(iterations):
                width = settings['width']
                height = settings['height']
                gate = settings['gate']
                strategy = 'new'
                starting_position = settings['starting_position']
                seed = 1245
                generator = PGenerator(width, height, gate, starting_position, seed)
                parameters = one_set['parameters']
                rng_seed = time.mktime(time.localtime())
                model = PModel(n, width, height, gate, strategy, generator, parameters, rng_seed=rng_seed)
                cnt = 0
                while model.running and cnt < 500:
                    model.step()
                    cnt += 1
                container_n.append(model.epoch)
                avg_in_n += model.epoch
                x += 1
            print("\t\t\t\t\t\tset", z, "finished, agent", n)
            container_avg.append(avg_in_n/iterations)
            data.append(container_n)
            y += 1
        filehandler_avg = open('out/nn2/avgA_' + one_set['parameters']['name'] + '.obj', 'wb')
        filehandler_data = open('out/nn2/dataA_' + one_set['parameters']['name'] + '.obj', 'wb')
        pickle.dump(container_avg, filehandler_avg)
        pickle.dump(data, filehandler_data)
        filehandler_avg = open('out/nn2/avgA_' + one_set['parameters']['name'] + '.obj', 'rb')
        filehandler_data = open('out/nn2/dataA_' + one_set['parameters']['name'] + '.obj', 'rb')
        container_avg = pickle.load(filehandler_avg)
        container_n = pickle.load(filehandler_data)
        # plt.plot(container_avg)
        # plt.boxplot(data)#, label=str(z))
        # plt.title("TET depending number of agents")
        # plt.ylabel("TET")
        # plt.xlabel("Number of agents in tens")
        # plt.savefig('out/nn/figG' + str(n) + "0" + str(x) + "-" + str(y) + "-" + str(z) + '.pdf')
        # plt.show()
        z += 1

    # Creating axes instance


def depickle():
    filep = 'out/n_depend/'
    names = ['1', '2', '3', '33', '4', '5', '55', '6']
    data = []
    avg = []
    for number in names:
        f_avg = open(filep +'avgB_' + number + '.obj', 'rb')
        f_data = open(filep + 'dataB_' + number + '.obj', 'rb')
        a = pickle.load(f_avg)
        d = pickle.load(f_data)
        avg.append(a)
        data.append(d)
        plt.plot(a, label=number)
        plt.legend()
    #for d in avg:
    #    plt.plot(d)
    plt.show()

def depickle2():
    filep = 'out/nn2/'
    names = ['1', '2', '3', '33', '4', '5', '55', '6']
    names2 = ['2', '3', '33']
    names3 = [ '6', '55']
    data = []
    avg = []
    min2 = 1111111111111
    min3 = 1111111111111
    max2 = 0
    max3 = 0
    for number in names2:
        f_avg = open(filep + 'avgA_' + number + '.obj', 'rb')
        f_data = open(filep + 'dataA_' + number + '.obj', 'rb')
        d = pickle.load(f_data)
        max2 = max(max2, max(d[-1]))
        min2 = min(min2, min(d[0]))
    for number in names3:
        f_avg = open(filep + 'avgA_' + number + '.obj', 'rb')
        f_data = open(filep + 'dataA_' + number + '.obj', 'rb')
        d = pickle.load(f_data)
        max3 = max(max3, max(d[-1]))
        min3 = min(min3, min(d[0]))
    min2 = min(min2, min3)
    max2 = max(max2, max3)
    print(min2, max3)
    fig, ax = plt.subplots()
    ax.set_ylim(min2, max2)
    for number in names2:
        f_avg = open(filep + 'avgA_' + number + '.obj', 'rb')
        f_data = open(filep + 'dataA_' + number + '.obj', 'rb')
        a = pickle.load(f_avg)
        d = pickle.load(f_data)
        a[0] = a[1]
        avg.append(a)
        data.append(d)
        plt.title("TET depends on number of agents, S1.1")
        plt.ylabel('TET')
        plt.xlabel('Number of agents in tens')

        if number == '3':
            box_plot(ax, d, 'red', 'tan')
        elif number == '2':
            box_plot(ax, d, 'green', 'yellow')
        else:
            box_plot(ax, d, 'blue', 'cyan')
    plt.savefig('out/nn2/names2-1-3-33alpha.pdf')
    plt.show()
    data = []
    avg = []
    fig, ax = plt.subplots()
    ax.set_ylim(min2, max2)
    for number in names3:
        print(number)
        f_avg = open(filep +'avgA_' + number + '.obj', 'rb')
        f_data = open(filep + 'dataA_' + number + '.obj', 'rb')
        a = pickle.load(f_avg)
        d = pickle.load(f_data)
        avg.append(a)
        data.append(d)
        plt.title("TET depends on number of agents, S1.2")
        plt.ylabel('TET')
        plt.xlabel('Number of agents in tens')
        if number == '55':
            box_plot(ax, d, 'red', 'tan')
        elif number == '5':
            box_plot(ax, d, 'green', 'yellow')
        else:
            box_plot(ax, d, 'blue', 'cyan')
    plt.savefig('out/nn2/names3-6-55.pdf')
    plt.show()


def box_plot(ax, data, edge_color, fill_color):

    bp = ax.boxplot(data, patch_artist=True)
    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    if edge_color == 'green':
        for patch in bp['boxes']:
            patch.set(facecolor=fill_color, alpha=0.1)
    else:
        for patch in bp['boxes']:
            patch.set(facecolor=fill_color)


def n_ks(settings):
    width = settings['width']
    height = settings['height']
    gate = settings['gate']
    strategy = 'new'
    starting_position = settings['starting_position']
    seed = 1245
    generator = PGenerator(width, height, gate, starting_position, seed)
    n = 70
    S = [i for i in np.arange(1.0, 5.2, 0.2)]
    iterations = 30
    good = ['1', '2', '3', '33']
    # ['4', '55', '6']
    settings1 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '1'
    }}
    settings2 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '2'
    }}
    settings3 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '3'
    }}
    settings33 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '33'
    }}
    settings4 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '4'
    }}
    settings5 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '5'
    }}
    settings55 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '55'
    }}
    settings6 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '6'
    }}
    all_sets = [settings1, settings2, settings3, settings33]
    x = 0
    y = 0
    z = 0

    for one_set in all_sets:
        container_avg = []
        for s in S:
            one_set['parameters']['S'] = s
            parameters = one_set['parameters']
            avg_in_n = 0
            for i in range(iterations):
                rng_seed = time.mktime(time.localtime())
                model = PModel(n, width, height, gate, strategy, generator, parameters, rng_seed=rng_seed)
                cnt = 0
                while model.running and cnt < 500:
                    model.step()
                    cnt += 1
                avg_in_n += model.epoch
                x += 1
            print("\t\t\t\t\t\tset", z, "finished, agent", n)
            container_avg.append(int(avg_in_n/iterations))
            y += 1
        filehandler_avg = open('out/nns/avgA_' + one_set['parameters']['name'] + '.obj', 'wb')
        pickle.dump(container_avg, filehandler_avg)
        # filehandler_avg = open('out/nns/avgA_' + one_set['parameters']['name'] + '.obj', 'rb')
        # container_avg = pickle.load(filehandler_avg)
        plt.plot(S, container_avg,
                 label="kO=" + str(one_set['parameters']['O']) + ", " +
                        "f=" + str(one_set['parameters']['F']) + ", " +
                        "kD=0.5")
        plt.legend()
        plt.title("TET depends on kO and friction")
        plt.ylabel("TET")
        plt.xlabel("kS")
        plt.savefig('out/nns/figA' + str(n) + "0" + str(iterations) + "0" + str(x) + "-" + str(y) + "-" + str(z) +
                    '.pdf')
        plt.show()
        z += 1

    # Creating axes instance

def depickle_nns():
    settings1 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '1'
    }}
    settings2 = {"parameters": {
        'S': 1.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '2'
    }}
    settings3 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '3'
    }}
    settings33 = {"parameters": {
        'S': 1.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '33'
    }}
    settings4 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.1,
        'name': '4'
    }}
    settings5 = {"parameters": {
        'S': 3.5,
        'O': 0.1,
        'D': 0.5,
        'F': 0.9,
        'name': '5'
    }}
    settings55 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.1,
        'name': '55'
    }}
    settings6 = {"parameters": {
        'S': 3.5,
        'O': 0.9,
        'D': 0.5,
        'F': 0.9,
        'name': '6'
    }}
    S = [i for i in np.arange(1.0, 5.2, 0.2)]
    for one_set in [settings1, settings2, settings3, settings33]:
        filehandler_avg = open('out/nns/avgA_' + one_set['parameters']['name'] + '.obj', 'rb')
        container_avg = pickle.load(filehandler_avg)
        plt.plot(S, container_avg,
                 label="kO=" + str(one_set['parameters']['O']) + ", " +
                       "f=" + str(one_set['parameters']['F']) + ", " +
                       "kD=0.5")
        plt.legend(loc='lower right', bbox_to_anchor=(1, 0.1))
        plt.title("TET depends on kO and friction")
        plt.ylabel("TET")
        plt.xlabel("kS")
    plt.savefig('out/nns/figX3.pdf')
    plt.show()

if __name__ == '__main__':
    #sampleEvaluation()

    # model = simple()
    # test()
    # model = medium()

    #model = large()
    #
    run = False
    if run:
        tet = []
        for i in range(0, 1000):
            if i % 128 == 0:
                print(i)
            model = simpleHetero()
            while model.running:
                model.step()
            tet.append(model.epoch)
        f = open('out/hetero/homoB2.obj', 'wb')
        pickle.dump(tet, f)
    else:
        first = False
        if first:
            f1 = open('out/hetero/homoA2.obj', 'rb')
            f2 = open('out/hetero/heteroA2.obj', 'rb')
            d1 = pickle.load(f1)
            d2 = pickle.load(f2)
            plt.hist(d1, range=(80, 95), bins=10, label='Homogeneous kO', alpha=.7, edgecolor='red')
            plt.ylim(0, 400)
            plt.hist(d2, range=(80, 95), bins=10, label="Heterogeneous kO", alpha=.7, edgecolor='yellow')
            plt.title("Heterogeneity in parameter kO, low friction")
            plt.xlabel("TET")
            plt.legend()
            plt.savefig("out/hetero/heteroE_A2.pdf")
            plt.show()
        else:
            f1 = open('out/hetero/homoB2.obj', 'rb')
            f2 = open('out/hetero/heteroB2.obj', 'rb')
            d1 = pickle.load(f1)
            d2 = pickle.load(f2)
            plt.hist(d1, range=(80, 130), bins=10, label='Homogeneous kO', alpha=.7, edgecolor='red')
            plt.ylim(0, 600)
            plt.hist(d2, range=(80, 130), bins=10, label="Heterogeneous kO", alpha=.7, edgecolor='yellow')
            plt.title("Heterogeneity in parameter kO, high friction")
            plt.xlabel("TET")
            plt.legend()
            plt.savefig("out/hetero/heteroE_B2.pdf")
            plt.show()


    # for repeat in range(100):
    #     batch_param_comb()
    # sampleEvaluation()
    settings = {
        "n": 70,
        "width": 15,
        "height": 15,
        "gate": (0, 8),
        "strategy": "new",
        "starting_position": "far",
        "seed": 1245,
        "generator": None,
        "parameters": {
            'S': 2.5,
            'O': 0.3,
            'D': 0.5,
            'F': 0.5
        }
    }
    filepath = 'out/hmap15_(s25-o03-d05-f05-n70)'
    # n_dep(settings)
    #depickle2()
    #n_ks(settings)
    #depickle_nns()
    #batch_sample(settings, iterations=100, max_iterations=500, filepath=filepath)

from mesa import Agent, Model
import math
from indicators import BLOCKED


class PCell(Agent):
    """
    Class representing one cell in floor field model. This cell holds up to one
    agent and static value - distance to exit. In each step of schedule cell
    decides which PAgent will enter this cell. Uses RNG from PModel.
    """

    def __init__(self, unique_id, model: Model, static, color):
        """
        :param unique_id: u_id for scheduler
        :param model: PModel reference
        :param static: distance to exit
        :param color: color representation for Mesa Grid visualization
        """
        super().__init__(unique_id, model)
        self.tag = "Cell" + str(unique_id)
        self.q = []
        self.static = static
        self.color = color
        self.n_steps = 0
        self.agents_stayed = False

    def add(self, agent):
        """
        :param agent: add agent to queue of agents wanting to enter this cell
        """
        self.q.append(agent)

    def start(self):
        """
        Iterate queue and select none or one agent to enter this cell in next
        time frame. Create bonds if this cell is occupied.
        """
        self.agents_stayed = False
        if len(self.q) > 0:
            winner = self.select_winning_agent()
            if winner:
                winner.next = self.pos
                occupant = self.model.get_agent(self.pos, "Pedestrian")
                if occupant:
                    if occupant != winner:
                        occupant.tail = winner
                        winner.head = occupant
                    else:
                        winner.head = winner
                else:
                    winner.head = None
            self.q = []

    def step(self):
        pass

    def select_winning_agent(self):
        """
        Choose PAgent/s with highest aggressivity. If there are two or more
        PAgents with equal aggressivity, friction equation plays a role.
        :return: None if no PAgent will enter this cell. Else PAgent that will
        enter this cell
        """
        opponents = []
        max_agent = max(self.q, key=lambda pagent: pagent.aggressivity)
        max_a = max_agent.aggressivity
        for agent in self.q:
            # select PAgents with approx. equal aggressivity to highest aggr.
            if math.isclose(max_a, agent.aggressivity):
                opponents.append(agent)
        if len(opponents) == 1:
            return opponents[0]
        else:
            """
            With probability P = friction * ( 1 - max_a ) no PAgent will enter 
            this cell and all will be blocked.
            With  ( 1 - P ) probability one of the PAgents with same aggr. will
            be randomly chosen to enter this cell.
            """
            all_stay_probability = self.model.friction * (1 - max_a)
            one_moves_probability = 1 - all_stay_probability
            all_stay = self.random.choices([True, False],
                                           weights=[all_stay_probability,
                                                    one_moves_probability])[0]
            if all_stay:
                for agent in opponents:
                    agent.color = BLOCKED
                self.agents_stayed = True
                self.model.blocks_total += 1
                return None
            else:
                return self.random.choice(opponents)
